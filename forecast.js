const key = "1c49604a573397768208e4c6172d153d";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base + query);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error("status code" + response.status);
    }
}

getForecast("mumbai")
    .then(data=> console.log(data))
    .catch(err=> console.log(err));